package edu.itpu.project.controller;

import edu.itpu.project.service.UserService;
import edu.itpu.project.user.User;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.Scanner;

public class UserControllerImpl implements UserController{
    private final UserService userService;

    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }
    @Override
    public void mainUserController() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("1. Display All Users");
            System.out.println("2. Add User");
            System.out.println("3. Remove User");
            System.out.println("0. Exit");
            System.out.print("Enter your choice: ");

            try {
                int choice = scanner.nextInt();
                scanner.nextLine(); // Consume the newline character

                switch (choice) {
                    case 1:
                        displayAllUsers();
                        break;
                    case 2:
                        addUser(scanner);
                        break;
                    case 3:
                        removeUser(scanner);
                        break;
                    case 0:
                        System.out.println("Exiting...");
                        return;
                    default:
                        System.out.println("Invalid choice. Try again.");
                        break;
                }
            } catch (Exception e) {
                System.err.println("Error: " + e.getMessage());
                scanner.nextLine(); // Clear the input buffer
            }
        }
    }

    private void displayAllUsers() {
        userService.getAllUsers().forEach(System.out::println);
    }

    private void addUser(Scanner scanner) {
        System.out.print("Enter user name: ");
        String name = scanner.nextLine();
        System.out.print("Enter password: ");
        String password = scanner.nextLine();
        System.out.print("Enter role: ");
        String role = scanner.nextLine();

        userService.addUser(new User(name, password, role));
        System.out.println("User added successfully!");
    }

    private void removeUser(Scanner scanner) throws UserPrincipalNotFoundException {
        System.out.print("Enter the name of the user to remove: ");
        String name = scanner.nextLine();

        userService.removeUser(name);
        System.out.println("User removed successfully!");
    }
}
