package edu.itpu.project.controller;

public class ControllerImpl implements Controller {
    private final DishesController dishesController;
    private final BedLinenController bedLinenController;

    public ControllerImpl(DishesController dishesController, BedLinenController bedLinenController) {
        this.dishesController = dishesController;
        this.bedLinenController = bedLinenController;
    }


    @Override
    public void displayById(String id) {
        dishesController.displayById(id);
        bedLinenController.displayById(id);
    }

    @Override
    public void displayByName(String name) {
        dishesController.displayByName(name);
        bedLinenController.displayByName(name);
    }

    @Override
    public void displayAll() {
        dishesController.displayAll();
        bedLinenController.displayAll();
    }
}
