package edu.itpu.project.controller;

import edu.itpu.project.service.BedLinenService;

public class BedLinenController implements Controller {
    private final BedLinenService service;

    public BedLinenController(BedLinenService service) {
        this.service = service;
    }


    @Override
    public void displayById(String id) {
        var bedLinens = service.getById(id);
        for (var bedLinen : bedLinens) {
            System.out.println(bedLinen);
        }
    }

    @Override
    public void displayByName(String name) {
        var bedLinens = service.getByName(name);
        for (var bedLinen : bedLinens) {
            System.out.println(bedLinen);
        }
    }

    @Override
    public void displayAll() {
        var bedLinens = service.getAll();
        for (var bedLinen : bedLinens) {
            System.out.println(bedLinen);
        }
    }
}
