package edu.itpu.project.controller;

import edu.itpu.project.service.DishesService;

public class DishesController implements Controller {
    private final DishesService service;

    public DishesController(DishesService service) {
        this.service = service;
    }


    @Override
    public void displayById(String id) {
        var dishes = service.getById(id);
        for (var dish : dishes) {
            System.out.println(dish);
        }
    }

    @Override
    public void displayByName(String name) {
        var dishes = service.getByName(name);
        for (var dish : dishes) {
            System.out.println(dish);
        }
    }

    @Override
    public void displayAll() {
        var dishes = service.getAll();
        for (var dish : dishes) {
            System.out.println(dish);
        }
    }
}
