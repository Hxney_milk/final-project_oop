package edu.itpu.project.controller;

public interface Controller {
    void displayById(String id);
    void displayByName(String name);
    void displayAll();
}
