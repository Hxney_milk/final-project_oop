package edu.itpu.project.service;

import edu.itpu.project.dao.DishesDao;
import edu.itpu.project.entity.Dishes;

import java.util.ArrayList;
import java.util.List;

public class DishesService implements ProductService<Dishes> {
    private List<Dishes> dishes;
    private final DishesDao dao;

    public DishesService(DishesDao dao) {
        this.dao = dao;
    }

    @Override
    public List<Dishes> getByName(String name) {
        List<Dishes> list = new ArrayList<>();
        dishes = dao.all();
        for (var dish : dishes) {
            if (dish.getName().contains(name))
                list.add(dish);
        }
        return list;
    }

    @Override
    public List<Dishes> getById(String id) {
        List<Dishes> list = new ArrayList<>();
        dishes = dao.all();
        for (var dish : dishes) {
            if (dish.getId().equals(id))
                list.add(dish);
        }
        return list;
    }

    @Override
    public List<Dishes> getAll() {
        return dao.all();
    }
}
