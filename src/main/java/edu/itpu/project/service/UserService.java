package edu.itpu.project.service;


import edu.itpu.project.dao.UserDao;
import edu.itpu.project.user.User;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class UserService {
    private final List<User> users;
    private final UserDao userDao;

    public UserService( UserDao userDao) {
        this.users = new ArrayList<>();
        this.userDao = userDao;
    }

    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    public void addUser(User user) {
        userDao.addUser(user);
    }

    public void removeUser(String name) throws UserPrincipalNotFoundException {
        userDao.removeUser(name);
    }
}
