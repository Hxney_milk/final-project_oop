package edu.itpu.project.service;

import edu.itpu.project.dao.BedLinenDao;
import edu.itpu.project.entity.BedLinen;

import java.util.ArrayList;
import java.util.List;

public class BedLinenService implements ProductService<BedLinen> {
    private List<BedLinen> dishes;
    private final BedLinenDao dao;

    public BedLinenService(BedLinenDao dao) {
        this.dao = dao;
    }

    @Override
    public List<BedLinen> getByName(String name) {
        List<BedLinen> list = new ArrayList<>();
        dishes = dao.all();
        for (var dish : dishes) {
            if (dish.getName().contains(name))
                list.add(dish);
        }
        return list;
    }

    @Override
    public List<BedLinen> getById(String id) {
        List<BedLinen> list = new ArrayList<>();
        dishes = dao.all();
        for (var dish : dishes) {
            if (dish.getId().equals(id))
                list.add(dish);
        }
        return list;
    }

    @Override
    public List<BedLinen> getAll() {
        return dao.all();
    }
}
