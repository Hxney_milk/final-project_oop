package edu.itpu.project.service;

import edu.itpu.project.entity.Product;

import java.util.List;

public interface ProductService<T extends Product> {
    List<T> getByName(String name);

    List<T> getById(String id);

    List<T> getAll();
}
