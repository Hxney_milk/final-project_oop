package edu.itpu.project;

import edu.itpu.project.controller.*;
import edu.itpu.project.dao.BedLinenDao;
import edu.itpu.project.dao.DishesDao;
import edu.itpu.project.dao.UserDao;
import edu.itpu.project.service.BedLinenService;
import edu.itpu.project.service.DishesService;
import edu.itpu.project.service.UserService;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        DishesDao dishesDao = new DishesDao();
        BedLinenDao bedLinenDao = new BedLinenDao();
        DishesService dishesService = new DishesService(dishesDao);
        BedLinenService bedLinenService = new BedLinenService(bedLinenDao);
        DishesController dishesController = new DishesController(dishesService);
        BedLinenController bedLinenController = new BedLinenController(bedLinenService);
        Controller controller = new ControllerImpl(dishesController, bedLinenController);
        UserDao userDao = new UserDao();
        UserService userService = new UserService(userDao);
        UserController userController = new UserControllerImpl(userService);

        System.out.println("Welcome to \"Bed Linens and Dishes Warehouse\"");
        System.out.println("Version: 1.0");
        System.out.println("Created on: 2023-06-03\n");

        System.out.println("Developer Information:");
        System.out.println("Name: Azizbek Sobirov");
        System.out.println("Email: azizbek_sobirov@student.itpu.uz\n");

        Scanner scanner = new Scanner(System.in);
        boolean flag = false;

        while (!flag) {
            System.out.println("Commands (Menu):");
            System.out.println("1. Display products by ID");
            System.out.println("2. Display products by name");
            System.out.println("3. Display all products");
            System.out.println("4. Create User");
            System.out.println("0. Exit\n");

            try {
                System.out.print(">> ");
                int number = scanner.nextInt();

                switch (number) {
                    case 1:
                        System.out.print("Enter product ID: ");
                        scanner.nextLine();
                        String id = scanner.nextLine();
                        controller.displayById(id);
                        break;
                    case 2:
                        System.out.print("Enter product name: ");
                        scanner.nextLine();
                        String name = scanner.nextLine();
                        controller.displayByName(name);
                        break;
                    case 3:
                        controller.displayAll();
                        break;
                    case 4:
                        userController.mainUserController();
                        break;
                    case 0:
                        flag = true;
                        break;
                    default:
                        System.out.println("Invalid choice! Please try again.");
                }

            } catch (InputMismatchException e) {
                System.out.println("Error: Invalid input.\n");
                scanner.next();
            }
            System.out.println();
        }

        System.out.println("Exiting the application.");
        scanner.close();
    }
}