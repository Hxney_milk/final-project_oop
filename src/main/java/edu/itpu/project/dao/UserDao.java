package edu.itpu.project.dao;


import edu.itpu.project.user.User;

import java.io.*;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    private static final String CSV_FILE_PATH = "src/main/resources/user.csv";
    private final List<User> users;

    public UserDao() {
        this.users = new ArrayList<>();
        loadUsersFromCsv();
    }

    private void loadUsersFromCsv() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(CSV_FILE_PATH))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] userData = line.split(",");
                String name = userData[0];
                String password = userData[1];
                String role = userData[2];

                User user = new User(name, password, role);
                users.add(user);
            }
        } catch (FileNotFoundException e) {
            System.err.println("CSV file not found. Creating a new one.");
        } catch (IOException e) {
            throw new RuntimeException("Error reading CSV file.", e);
        }
    }

    private void saveUsersToCsv() {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(CSV_FILE_PATH))) {
            for (User user : users) {
                bufferedWriter.write(user.getName() + "," + user.getPassword() + "," + user.getRole());
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException("Error writing to CSV file.", e);
        }
    }

    public List<User> getAllUsers() {
        return new ArrayList<>(users);
    }

    public void addUser(User user) {
        users.add(user);
        saveUsersToCsv();
    }

    public void removeUser(String name) throws UserPrincipalNotFoundException {
        User userToRemove = null;

        for (User user : users) {
            if (user.getName().equals(name)) {
                userToRemove = user;
                break;
            }
        }

        if (userToRemove != null) {
            users.remove(userToRemove);
            saveUsersToCsv();
        } else {
            throw new UserPrincipalNotFoundException("User with name '" + name + "' not found.");
        }
    }
}
