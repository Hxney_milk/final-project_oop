package edu.itpu.project.dao;

import edu.itpu.project.entity.BedLinen;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BedLinenDao implements ProductDao<BedLinen> {
    private final String path = "src/main/resources/bedlinen.csv";
    @Override
    public List<BedLinen> all() {
        List<BedLinen> bedLinens = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            boolean skipLine = true;
            while ((line = br.readLine()) != null) {
                if (skipLine) {
                    skipLine = false;
                    continue;
                }

                String[] data = line.split(",");
                if (data.length == 6) {
                    String name = data[0].trim();
                    String id = data[1].trim();
                    String size = data[2].trim();
                    String color = data[3].trim();
                    int quantity = Integer.parseInt(data[4].trim());
                    int price = Integer.parseInt(data[5].trim());

                    BedLinen bedLinen = new BedLinen(name, id, quantity, price, size, color);
                    bedLinens.add(bedLinen);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bedLinens;
    }
}
