package edu.itpu.project.dao;

import edu.itpu.project.entity.Product;

import java.util.List;

public interface ProductDao<T extends Product> {
    List<T> all();
}
