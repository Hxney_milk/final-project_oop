package edu.itpu.project.dao;

import edu.itpu.project.entity.Dishes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DishesDao implements ProductDao<Dishes> {
    private final String path = "src/main/resources/dishes.csv";

    @Override
    public List<Dishes> all() {
        List<Dishes> dishes = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            boolean skipLine = true;
            while ((line = br.readLine()) != null) {
                if (skipLine) {
                    skipLine = false;
                    continue;
                }

                String[] data = line.split(",");
                if (data.length == 6) {
                    String name = data[0].trim();
                    String id = data[1].trim();
                    String color = data[2].trim();
                    String category = data[3].trim();
                    int quantity = Integer.parseInt(data[4].trim());
                    int price = Integer.parseInt(data[5].trim());

                    Dishes dish = new Dishes(name, id, quantity, price, color, category);
                    dishes.add(dish);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return dishes;
    }
}
