package edu.itpu.project.entity;

import java.util.Objects;

public class Dishes extends Product {
    private final String color;
    private final String category;

    public Dishes(String name, String id, int quantity, int price, String color, String category) {
        super(name, id, quantity, price);
        this.color = color;
        this.category = category;
    }

    public String getColor() {
        return color;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Dishes dishes = (Dishes) o;
        return Objects.equals(color, dishes.color) && Objects.equals(category, dishes.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), color, category);
    }

    @Override
    public String toString() {
        return "Dishes{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", color='" + color + '\'' +
                ", category='" + category + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }

    @Override
    public int compareTo(Product o) {
        return this.getName().compareTo(o.getName());
    }
}
