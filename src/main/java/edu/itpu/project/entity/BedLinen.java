package edu.itpu.project.entity;

import java.util.Objects;

public class BedLinen extends Product {
    private final String size;
    private final String color;

    public BedLinen(String name, String id, int quantity, int price, String size, String color) {
        super(name, id, quantity, price);
        this.size = size;
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BedLinen bedLinen = (BedLinen) o;
        return Objects.equals(size, bedLinen.size) && Objects.equals(color, bedLinen.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), size, color);
    }

    @Override
    public String toString() {
        return "BedLinen{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", size='" + size + '\'' +
                ", color='" + color + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }

    @Override
    public int compareTo(Product o) {
        return this.getName().compareTo(o.getName());
    }
}
