# Report
#### by Azizbek Sobirov

| #   | Stage                      | Start date | End date | Comment                                                     |
|-----|----------------------------|------------|----------|-------------------------------------------------------------|
| 1   | Task Clarification         | 26 May     | 29 May   |                                                             |
|     | Analyze                    | 26 May     | 28 May   | - Received and analyzed the task                            |
|     | Initial commit             | 29 May     | 29 May   | - Made the first initial commit with a blank project        |
| 2   | Software development       | 29 May     | 2 June   |                                                             |
|     | Created the entity package | 29 May     | 29 May   | - Added three classes (Product, BedLinen, and Dishes)       |
|     | DAO layer                  | 29 May     | 29 May   | - Created DAO interface and implementations                 |
|     | Service layer              | 29 May     | 31 May   | - Created service package with interface and classes        |
|     | Controller layer           | 29 May     | 31 May   | - Added new interface and classes to the controller package |
|     | Project setup              | 31 May     | 2 June   | - Made the project runnable from Main class                 |
| 3   | Presentation               | 2 June     | 9 June   |                                                             |
